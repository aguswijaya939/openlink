# Easy Open Link
Easy Open Link is a small Android app which makes it easy to open links from text documents via the share function of many apps. No more cumbersome copy and paste. Easy Open Link also allows you to open several links at the same time.

* Roughly select the URL(s). It does not matter if the selection also contains additional text or white spaces.
* Press the "share" symbol.
* Select "open link"

The share function has become a common feature in more recent versions of Android. This app requires Android 1.5 or newer, but is most useful with Android 4.0 or later.

Instruction video:

[![click to start video](promo/0.jpg)](http://www.youtube.com/watch?v=cTIn11Zw1cc "Start video for 'Easy Open Link'")

# Permission
Easy Open Link requires the RECEIVE_BOOT_COMPLETED permission. I have written [an article](docs/permissions/RECEIVE_BOOT_COMPLETED.md) about the reasons for this.

# Binary
Binary releases (aka APKs) are available on:

[![Get it on Google Play](promo/en-play-badge_resized.png)](https://play.google.com/store/apps/details?id=de.audioattack.openlink)
[<img src="promo/F-Droid-button_get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/repository/browse/?fdid=de.audioattack.openlink)

# License
This software is licensed under the terms of the [GPL v3](https://gitlab.com/marc.nause/openlink/raw/master/LICENSE).

![logo GPL v3](promo/GPLv3.png)

# Donate
[![Donate using Liberapay](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/nause_marc/donate)